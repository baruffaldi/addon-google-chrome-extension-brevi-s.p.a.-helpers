﻿/***********************************************/
/***********************************************/
/*** USEFUL FUNCTIONS ***/
/***********************************************/
/***********************************************/
var tipTimeout = 2700,
	parseProductRow = function(row, a) {
		row = $$$(row);
		var p_id = row.children('td').children('a')[0].href.replace('scheda_','').replace('.htm','').split('/').pop(),
			p_total = parseFloat(row.children('td')[4].innerText.replace(',','.')) || null,
			p_quantity = parseInt(row.children('td.prezzotd').children('input.numbcar').val()),
			p_price = (typeof row.children('td').children('div') != 'undefined') ? parseFloat(row.children('td').children('div').get(-1).innerText.substr(2).replace(',','.')) : p_total/p_quantity,
			p_old_price = (typeof row.children('td').children('div') != 'undefined') ? parseFloat(row.children('td').children('div')[0].innerText.substr(2).replace(',','.')) : p_total/p_quantity,
			p_dis = $$$($$$('#listato tr[class!="grey"]')[a+1]).children('td').children('table.dis').children('tbody').children('tr')[1].childNodes;
		return {
		  row_id: a,
		  disp_sede: parseInt(p_dis[0].innerText) || 0,
		  imp_sede: parseInt(p_dis[1].innerText) || 0,
		  disp_cc: parseInt(p_dis[3].innerText) || 0,
		  imp_cc: parseInt(p_dis[4].innerText) || 0,
		  id: p_id,
		  cat_id: parseInt(p_id),
		  product_id: parseInt(p_id.split('.')[1]),
		  title: row.children('td').children('a').children('strong.blue')[0].innerText,
		  desc: row.children('td').children('a').children('span.desc')[0].innerText,
		  price: parseFloat(p_price.toFixed(2)),
		  price_recharge: 0,
		  price_iva: 0,
		  recharge: 0,
		  old_price: parseFloat(p_old_price.toFixed(2)),
		  discount: row.children('td')[3].innerText || "0%",
		  discount_amount: parseFloat((p_old_price - p_price).toFixed(2)),
		  quantity: p_quantity
		};
	},
	addToCart = function(p){
		cart = getCart();
		var ex = $$$.grep(cart, function(e){ return e.id == p.id; });
		if (ex.length >= 1) {
			if (cart.indexOf(ex[0]) != -1) {
				cart[cart.indexOf(ex[0])].quantity += p.quantity;
			} else { cart.push(p); }
		} else cart.push(p);
		var cart_json = JSON.stringify(parseProducts(cart));
		localStorage.setItem('cart', cart_json);
	},
	modifyProductOnCart = function(p){
		cart = getCart();
		var ex = $$$.grep(cart, function(e){ return e.id == p.id; });
		if (ex.length >= 1) {
			if (cart.indexOf(ex[0]) != -1) {
				cart[cart.indexOf(ex[0])].quantity = p.quantity;
			}
		}
		var cart_json = JSON.stringify(parseProducts(cart));
		localStorage.setItem('cart', cart_json);
	},
	removeFromCart = function(p){
		cart = getCart();
		$$$.grep(cart, function(e){
			if (e.id == p.id) {
				var position=cart.indexOf(e);
				if ( ~position ) cart.splice(position, 1);
				return true;
			}
		});
		var cart_json = JSON.stringify(parseProducts(cart));
		localStorage.setItem('cart', cart_json);
	},
	parseProducts = function(products){
		var category;
		if ($$$('form[name="catego"] select option[selected="selected"]').length >= 1)
			category = $$$('form[name="catego"] select option[selected="selected"]')[0].value.toLowerCase();
		if ($$$(products).length)
			$$$(products).each(function(a,p){
				if (products[a].cat == null || typeof products[a].cat == 'undefined' || products[a].cat == '')
					products[a].cat = category;
				
				if (!settings["price_handling_category_"+category])
					products[a].recharge = settings.price_handling_recharge;
				else products[a].recharge = settings["price_handling_category_"+category];
				products[a].price_iva = parseFloat((p.price / 100 * parseInt(settings.price_handling_iva)).toFixed(2));
				products[a].price_plus_iva = parseFloat((products[a].price + products[a].price_iva).toFixed(2));
				products[a].price_recharge = parseFloat((p.price / 100 * parseInt(products[a].recharge)).toFixed(2));
				products[a].price_recharge_iva = parseFloat(((products[a].price + products[a].price_recharge) / 100 * parseInt(settings.price_handling_iva)).toFixed(2));
				products[a].price_plus_recharge = parseFloat((products[a].price + products[a].price_recharge).toFixed(2));
				products[a].price_plus_recharge_iva = parseFloat((products[a].price + products[a].price_recharge + products[a].price_recharge_iva).toFixed(2));
				
				
				products[a].total_recharge = parseFloat((products[a].price_recharge * parseInt(p.quantity)).toFixed(2)) || products[a].price_recharge;
				products[a].total_iva = parseFloat((products[a].price_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_iva;
				products[a].total_recharge_iva = parseFloat((products[a].price_recharge_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_recharge_iva;
				products[a].total_plus_recharge = parseFloat((products[a].price_plus_recharge * parseInt(p.quantity)).toFixed(2)) || products[a].price_plus_recharge;
				products[a].total_plus_iva = parseFloat((products[a].price_plus_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_plus_iva;
				products[a].total_plus_recharge_iva = parseFloat((products[a].price_plus_recharge_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_plus_recharge_iva;
				products[a].total = parseFloat((p.price * p.quantity).toFixed(2)) || p.price;
			});
		return products;
	};

/***********************************************/
/***********************************************/
/*** BREVIZER LISTING INIT ***/
/***********************************************/
/***********************************************/
console.log("Brevizer listing init.");
chrome.extension.sendMessage({getSettings:true}, function(response) {
	settings = response["settings"];
	console.log("Brevizer listing settings loaded.");

	if (settings.enabled) {
		if (isLoggedIn() && isInAdministration()) {
			/***********************************************/
			/*** SORTING OPTIONS HANDLING ***/
			/***********************************************/
			if ($$$('#promo').length && window.location.pathname.indexOf('promo') == -1 && window.location.search.indexOf("promo=promo") == -1) {
				if (!$$$('#promo[checked="checked"]').length && settings.ui_listing_promo == 'y') {
					window.location.search = window.location.search+"&promo=promo"; return;
				}
			}
			if ($$$('#prz').length) {
				if ($$$('#prz')[0].value == settings.ui_listing_autosort) { $$$('#pr').click(); return; }
			}
			if ($$$('#listo').length) {
				if ($$$('#listo')[0].value == settings.ui_listing_light) { $$$('#list').click(); return; }
			}
			
			/** Fino a quando non fixano il bug sul sorting, è impossibile preimpostare anche la disponibilità dei prodotti 
			if ($$$('#dispo').length) {
				if ($$$('#dispo')[0].value == settings.ui_listing_availability) { $$$('#disp').click(); return; }
			}*/

			/***********************************************/
			/*** ENHANCED USER INTERFACE ***/
			/***********************************************/
			if (settings.ui_enhanced_view) {
				// Big thumbnails
				$$$('.imggif').css({width:100,height:100});
				$$$('.imggif').each(function(a,b){
					var n = b.src.split('/').pop();
					if (n != 'pix.png')
						b.src = b.src.split('/').slice(3,6).join('/')+'/100x100/'+n;
				});
				
				// Colourize available products
				$$$('.green').each(function(a,b){
				  var txt = b.innerText.toLowerCase();
				  if(txt.indexOf('disponibile in sede') != -1)// || txt.startsWith('disponibile in sede'))
					b.style.backgroundColor = 'orange';
				});
			}
		}
		
		var readyStateCheckInterval = setInterval(function() {
			if (document.readyState === "complete") {
				clearInterval(readyStateCheckInterval);
				
				if (isLoggedIn() && isInAdministration()) {
					/***********************************************/
					/*** STORE LISTING PRODUCTS ***/
					/***********************************************/
					if ($$$('#listato tr[class!="grey"]').length) {
						$$$('#listato tr[class!="grey"]').each(function(a,b){
						  if ($$$(b).children('td').length == 7 || $$$(b).children('td').length == 5) {
							if (typeof $$$(b).children('td').children('a')[0] != 'undefined')
								products.push(parseProductRow(b, a));
						  }
						});
					}
					
					/***********************************************/
					/*** CATCH CART NEW PRODUCTS ***/
					/***********************************************/
					$$$('.mycar').click(function(a){
						window.stop();
						var mod = $$$(a.currentTarget).parent().children()[0].name.indexOf('mod[') != -1,
							id = (mod)  ? $$$(a.currentTarget).parent().children()[0].name.replace('mod[','').replace(']','')
										: $$$(a.currentTarget).parent().children()[0].name.replace('car[','').replace(']',''),
							p = $$$.grep(products, function(e){ return e.id == id; });

						p[0].quantity = parseInt($$$(a.currentTarget).parent().children()[0].value||0);

						if (mod)
							modifyProductOnCart(p[0]);
						else addToCart(p[0]);
						window.setTimeout(function(){$$$(a.srcElement).click();}, 1000);
					});
					
					/***********************************************/
					/*** PRICE HANDLING ***/
					/***********************************************/
					if (settings.price_handling) {
						products = parseProducts(products);
						$$$(products).each(function(a,p){
							var cell = $$$($$$('#listato tr[class!="grey"]')[p.row_id]).children()[2],
								details = "Costo: "+p.price+" &euro;<br /> \
										  IVA: "+p.price_iva+" &euro;<br /> \
										  Costo IVATO: "+p.price_plus_iva+" &euro;<br /> \
										  [CLIENTE] Costo: "+p.price_plus_recharge+" &euro;<br /> \
										  [CLIENTE] IVA: "+p.price_recharge_iva+" &euro;<br /> \
										  [CLIENTE] Costo IVATO: "+p.price_plus_recharge_iva+" &euro;";
							if (settings.price_handling_calc_recharge)
								details += "<br />Categoria: "+(p.cat||'non disponibile')+"<br />Ricarico percentuale: "+(p.recharge)+"%<br /> \
										  Ricarico: "+(p.price_plus_recharge_iva-p.price_plus_iva).toFixed(2)+" &euro;";

							if (settings.price_handling_calc_recharge) {
								if (settings.price_handling_calc_iva)
									$$$(cell).children('div')[0].innerHTML = '<p class="tiny_text">Imp. '+p.price_plus_recharge + '&euro; +<br />IVA ' + p.price_recharge_iva + '&euro; =</p>' + p.price_plus_recharge_iva + ' &euro;';
								else $$$(cell).children('div')[0].innerHTML = p.price_plus_recharge + ' &euro; + IVA';
							} else {
								if (settings.price_handling_calc_iva)
									$$$(cell).children('div')[0].innerHTML = '<p class="tiny_text">Imp. '+p.price + '&euro; +<br />IVA '+p.price_iva+'&euro; =</p>'+p.price_plus_iva+' &euro;';
								else $$$(cell).children('div')[0].innerHTML = p.price + ' &euro; + IVA';
							}
							cell.innerHTML = cell.innerHTML + "<br /><img onclick='Tip(\""+details+"\");setTimeout(function(){UnTip()},"+tipTimeout+")' src='"+chrome.extension.getURL('/icons/point.png')+"' alt='*' />";
						});
					}
				}
			}
		}, 10);
		console.log("Brevizer listing ends.");
	}
});
if(chrome.runtime.lastError) { console.error(chrome.runtime.lastError); }