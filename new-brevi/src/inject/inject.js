﻿/***********************************************/
/***********************************************/
/*** USEFUL FUNCTIONS ***/
/***********************************************/
/***********************************************/
String.prototype.capitalize = function() {
    return this.trim().charAt(0).toUpperCase() + this.slice(1).toLowerCase();
}

String.prototype.camelize = function() {
    var a = this.split(' '),
        ret = [];
    for (var x in a){
        a[x] = a[x].capitalize();
    }
    return a.join(" ").trim();
}

var $$$ = jQuery,
	timestamp = function(){return new Date().getTime()},
	isLoggedIn = function(){
		return document.cookie.indexOf('commerce=') != -1 && document.cookie.indexOf('shop=') != -1
	},
	isInAdministration = function(){
		return (
			window.location.pathname.indexOf('index.php') != -1 ||
			window.location.pathname.indexOf('commerce.htm') != -1 ||
			window.location.pathname.indexOf('promo') != -1 ||
			window.location.pathname.indexOf('news') != -1 ||
			window.location.pathname.indexOf('eventi') != -1 ||
			window.location.pathname.indexOf('offerte') != -1 ||
			window.location.pathname.indexOf('ordini') != -1 ||
			window.location.pathname.indexOf('fatture') != -1 ||
			window.location.pathname.indexOf('scadenziario') != -1 ||
			window.location.pathname.indexOf('preferiti') != -1 ||
			window.location.pathname.indexOf('carrello') != -1 ||
			window.location.pathname.indexOf('personalizza') != -1 )
	},
	getCart = function(){
		return JSON.parse(localStorage.getItem('cart')||"[]");
	},
	getTotalsFromCart = function(){
		return getTotalsFromProducts(cart);
	},
	getTotalsFromProducts = function(prs){
		var total_recharge = 0,
			total_iva = 0,
			total_recharge_iva = 0,
			total_plus_recharge = 0,
			total_plus_recharge = 0,
			total_plus_iva = 0,
			total_plus_recharge_iva = 0,
			total = 0;
		$$$(prs).each(function(a,p){
			total_recharge = parseFloat((total_recharge + p.total_recharge).toFixed(2));
			total_iva = parseFloat((total_iva + p.total_iva).toFixed(2));
			total_recharge_iva = parseFloat((total_recharge_iva + p.total_recharge_iva).toFixed(2));
			total_plus_recharge = parseFloat((total_plus_recharge + p.total_plus_recharge).toFixed(2));
			total_plus_iva = parseFloat((total_plus_iva + p.total_plus_iva).toFixed(2));
			total_plus_recharge_iva = parseFloat((total_plus_recharge_iva + p.total_plus_recharge_iva).toFixed(2));
			total = parseFloat((total + p.total).toFixed(2));
		});
		return [total,total_iva,total_plus_iva,total_recharge,total_recharge_iva,total_plus_recharge,total_plus_recharge_iva];
	},
	toggleTip = function(el, msg){
		if (typeof el.tip_status == 'undefined' || !el.tip_status) {
			el.tip_status = true;
			Tip(msg);
		} else UnTip();
	},
	getFixedServiceBox = function(msg){
		$$$('#service_box')[0].innerHTML = msg;
		$$$('#service_box').css({
			position: 'fixed',
			top: 5,
			width: 300,
			margin: '0 auto',
			zIndex: 100,
			opacity: 0.9,
			boxShadow: '1px 1px 3px green',
			color: '#000000',
			borderRadius: '10px',
			border: '1px solid green',
			textShadow: '1px 1px 1px #ffffff',
			padding: '10px',
			fontSize: '12px',
			backgroundColor: 'rgb(0, 200, 0)',
			marginLeft: (window.innerWidth/2)-(jQuery('#service_box').width()/2)
		});
		$$$('#service_box').show();
	};

var blank_image_url = "data:image/gif;base64,R0lGODlhAQABAPAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

/***********************************************/
/***********************************************/
/*** BREVIZER INIT ***/
/***********************************************/
/***********************************************/
console.log("Brevizer inject init.");
var settings = [],
	categories = [],
	products = [],
	totals = [],
	cart = getCart(),
	local_settings = JSON.parse(localStorage.getItem('settings')||"{}");

if (typeof local_settings.ui_minimal != 'undefined' && local_settings.ui_minimal) {
	// Hide elements
	$$$('#id').hide();
	$$$('#homeheadcom').hide();
	$$$('#banners').hide();
	$$$('#commento').hide();
	$$$('#footer').hide();
}

chrome.extension.sendMessage({getSettings:true}, function(response) {
	settings = response["settings"];
	localStorage.setItem('settings', JSON.stringify(settings));
	console.log("Brevizer settings loaded.");
	console.log(settings);
	
	if (settings.enabled) {
		if (isLoggedIn() && isInAdministration()) {
			$$$(document.body).prepend($$$('<p id="service_box" class="hidden"></p>'))
			
			if (window.location.pathname.indexOf('commerce.htm') != -1)
				$$$('#centralcom').hide();
				
			if (settings.ui_listing_categories_alpha) {
				var options = $$$('select#cata option');
				var arr = options.map(function(_, o) { return { t: $$$(o).text(), v: o.value }; }).get();
				arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
				options.each(function(i, o) {
				  o.value = arr[i].v;
				  $$$(o).text(arr[i].t);
				});
			}

			/***********************************************/
			/*** GET CART TOTALS ***/
			/***********************************************/
			cart_totals = getTotalsFromCart();
				
			/***********************************************/
			/*** MINIMAL USER INTERFACE ***/
			/***********************************************/
			if (settings.ui_minimal) {
				var buttons = $$$('#buttons')[0],
					space = new Text(' ');
					
				// Move cart button
				var cart_el = $$$('<a></a>')[0];
					cart_el.href = carrello.href;
					cart_el.title = carrello.title;
					cart_el.innerText = carrello.innerText;
					cart_el.className = 'hbutton';
					cart_el.tabIndex = 21;
					cart_el.style.color = '#000000';
					cart_el.style.padding = '5px';
				if (cart_totals[0] >= parseFloat(settings.price_min_limit))
					cart_el.style.backgroundColor = 'rgb(0, 255, 0)';
				else
					cart_el.style.backgroundColor = 'rgb(255, 107, 107)';
				$$$(buttons).append(space);
				$$$(buttons).append(cart_el);
				
				// Create promo button
				var promo = $$$('<a></a>')[0];
					promo.href = '/promo';
					promo.innerText = 'Promozioni';
					promo.title = promo.innerText;
					promo.className = 'hbutton';
					promo.tabIndex = 2;
				$$$(buttons).prepend(space);
				$$$(buttons).prepend(promo);
				
				// Create home button
				var home = $$$('<a></a>')[0];
					home.href = $$$('#logo').parent()[0].href;
					home.title = $$$('#logo').parent()[0].title;
					home.innerText = 'Home';
					home.className = 'hbutton';
					home.tabIndex = 1;
				$$$(buttons).prepend(space);
				$$$(buttons).prepend(home);

				// Decorate elements
				$$$('#buttons').css('margin', '0 0 10px 0')
				$$$('#centralcom').css('margin', '0 0');
				$$$('.hbutton').css({
					border: '1px solid #cccccc',
					padding: '10px',
					borderRadius: '5px'
				});
				$$$('.green, .green span').css({width: '100%', textAlign: 'left'});
				
				// Hide elements
				$$$('#id').hide();
				$$$('#homeheadcom').hide();
				$$$('#banners').hide();
				$$$('#commento').hide();
				$$$('#footer').hide();
			}
			
			/***********************************************/
			/*** WIDE SCREEN USER INTERFACE ***/
			/***********************************************/
			if (settings.ui_wide) {
				$$$('#lateral').css({
					marginTop: 0,
					width: '22%'
				});
				$$$('#home963com').css({
					margin: '0 10px',
					padding: '0 10px',
					width: '97%'
				});
			}

			/***********************************************/
			/*** LOGO HANDLING ***/
			/***********************************************/
			if (settings.ui_hide_logo) {
				$$$('#logo').hide();
				$$$('#filiale').hide();
				$$$('#footer').hide();
			}
			if (settings.ui_logo_url) {
				$$$('#filiale')[0].innerText = $$$('a[href="/personalizza"]')[0].innerText;
				$$$('#filiale').show();
				$$$('#logo')[0].src = settings.ui_logo_url;
				$$$('#logo').show();
				if (settings.ui_wide)
					$$$('#logo').css({
						height:'auto',
						width:220,
						top:0,
						left:40,
						zIndex:99,
						position:'absolute'
					});
				else $$$('#logo').css({
						height:'auto',
						position:'absolute'
					});
				var readyStateCheckInterval = setInterval(function() {if (document.readyState === "complete") {clearInterval(readyStateCheckInterval);$$$('#lateral').css('margin-top', $$$('#logo').height()-20)}},30);
				$$$('#footer').hide();
			}

			/***********************************************/
			/*** HIDE ADVERTISMENT ***/
			/***********************************************/
			if (settings.ui_hide_ads) {
				$$$('*[id*="banner"][id!="banners"]').hide();
				$$$('*[id*="banner"] div[align="center"]').hide();
				$$$('img[src*="/media/images/banners/"]').hide();
			}

			/***********************************************/
			/*** HIDE NEWS ***/
			/***********************************************/
			if (settings.ui_hide_news) {
				$$$('#lastnews').hide();
				$$$('#centralcom ul').hide();
				$$$('a[href*="/news"]').hide();
			}
		}
		
		var readyStateCheckInterval = setInterval(function() {
			if (document.readyState === "complete") {
				clearInterval(readyStateCheckInterval);
				
				if (isLoggedIn() && isInAdministration()) {

					/***********************************************/
					/*** STORE PRODUCTS CATEGORIES ***/
					/***********************************************/
					if ($$$('form[name="catego"] select option').length){
						$$$('form[name="catego"] select option').each(function(a,b){
							categories.push({
								name: b.innerText.camelize(),
								id: b.value.toLowerCase()
							});
						});
						chrome.extension.sendMessage({
								setOption:{
									name:'price_handling_categories',
									value: JSON.stringify(categories)
								}},
							function(response) {
						});
					}

					/***********************************************/
					/*** AUTO LOGIN ***/
					/***********************************************/
					/** TODO: catch loading forms and hrefs and take note of destination URL
					 *        if login needed, login first and redirect then.
					$$$('a').click(function(a,b,c){			
						chrome.extension.sendMessage({
								setOption:{
									name:'gotta_redirect',
									value: a.href
								}},
							login
						);
					});*/
				}
				if (settings.persistent_login && $$$('#userid, #passwd, #ok_login').length >= 3) {
					console.log('Brevizer gotta login');
					var login = function(r){
						var login = $$$('#userid, #passwd, #ok_login');
						login[0].value = settings.login_username;
						login[1].value = settings.login_password;
						login[2].click();
					};
					/** NOT WORKING
					if (window.document.referrer.indexOf("//www.brevi.it") != -1) {
						chrome.extension.sendMessage({
								setOption:{
									name:'gotta_redirect',
									value: window.document.referrer
								}},
							login
						);
					} else */login(0);
				}
			}
		}, 10);
		console.log("Brevizer inject ends.");
		chrome.extension.sendMessage({addCart:['cart', JSON.stringify(parseProducts(getCart()))]}, function(response) {console.log("Brevizer cart saved.");});
	}
});
if(chrome.runtime.lastError) { console.error(chrome.runtime.lastError); }