/***********************************************/
/***********************************************/
/*** BREVIZER CART INIT ***/
/***********************************************/
/***********************************************/
console.log("Brevizer cart init.");

chrome.extension.sendMessage({getSettings:true}, function(response) {
	var settings = response["settings"];
	console.log("Brevizer cart settings loaded.");

	if (settings.enabled) {
		var readyStateCheckInterval = setInterval(function() {
			if (document.readyState === "complete") {
				clearInterval(readyStateCheckInterval);
				
				if (isLoggedIn() && isInAdministration()) {
					/***********************************************/
					/*** DISPLAY SAVE BUTTON ON CART ***/
					/***********************************************/
					$$$('#procediq').parent().prepend('<input type="button" style="float:right" id="salva" name="salva" value="Salva questo ordine" />');
					$$$('#salva').css({
					  color:'#ffffff',
					  borderRadius:'10px',
					  fontWeight:'bold',
					  backgroundColor:'red',
					  textTransform:'uppercase',
					  marginLeft:'5px',
					  padding:'0 5px',
					  height:'62px',
					  width:'154px',
					  boxShadow: '1px 1px 3px #000'
					});

					/***********************************************/
					/***  STORE CART TO EXTENSION CARTS STORAGE  ***/
					/***********************************************/
					$$$('#salva').click(function(){
						if ($$$('#listato tr[class!="grey"]').length) {
							$$$('#listato tr[class!="grey"]').each(function(a,b){
							  if ($$$(b).children('td').length == 7 || $$$(b).children('td').length == 5) {
								if (typeof $$$(b).children('td').children('a')[0] != 'undefined')
									products.push(parseProductRow(b, a));
							  }
							});
						}
						
						chrome.extension.sendMessage({addCart:['cart'+timestamp(), JSON.stringify(parseProducts(getCart()))]}, function(response) {
							if (response) {
								var delete_script = "";
								$$$('.trash').each(function(a,b){
									if (($$$('.trash').length-1)==a)
										delete_script += 'jQuery.post( "/carrello", {"'+b.name+'": "Compra"}, function(){localStorage.removeItem("cart");jQuery("#service_box").html("Fatto! Riavvio in corso...");setTimeout(function(){window.location.reload()},3000);} );';
									else delete_script += 'jQuery.post( "/carrello", {"'+b.name+'": "Compra"}, function(){} );';
								});
								msg = 'Carrello salvato.<br />Vuoi svuotare il carrello per effettuare altri acquisti?<br /><a href=\'javascript:jQuery("#service_box").html("Cancellazione in corso...");jQuery("div[id!=\\"service_box\\"]").hide();jQuery("#service_box").show();jQuery("#service_box *").show();'+delete_script+'\'>Si!</a><br /><a href="javascript:jQuery(\'#service_box\').hide();jQuery(\'#service_box\').html(\' \');">Lascia pure il carrello pieno</a>';
								getFixedServiceBox(msg);
							} else {
								msg = 'ERRORE<br />Impossibile salvare il carrello.<br />Vuoi ritentare ora?<br /><a href=\'javascript:jQuery("#salva").click();jQuery("#service_box *").show();\'>Si!</a><br /><a href="javascript:jQuery(\'#service_box\').hide();jQuery(\'#service_box\').html(\' \');">No, riproverò piu\' tardi</a>';
								getFixedServiceBox(msg);
							}
						});
						//localStorage.setItem('cart'+timestamp(), JSON.stringify(products));

					});
					
					/***********************************************/
					/*** CATCH CART PRODUCTS DELETION ***/
					/***********************************************/
					$$$('input[name*="elimina"]').click(function(a){
						var id = a.currentTarget.name.replace('elimina[','').replace('^1]',''),
							p = $$$.grep(products, function(e){ return e.id == id; });
						removeFromCart(p[0]);
					});
					
					/***********************************************/
					/*** GET SINGLE PRODUCT TOTAL PRICE HANDLING ***/
					/***********************************************/
					if (settings.price_handling) {
						products = parseProducts(products);
						$$$(products).each(function(a,p){
							var cell = $$$($$$('#listato tr[class!="grey"]')[p.row_id]).children()[4],
								details = "Costo: "+p.total+" &euro;<br /> \
										  IVA: "+p.total_iva+" &euro;<br /> \
										  Costo IVATO: "+p.total_plus_iva+" &euro;<br /> \
										  [CLIENTE] Costo: "+p.total_plus_recharge+" &euro;<br /> \
										  [CLIENTE] IVA: "+p.total_recharge_iva+" &euro;<br /> \
										  [CLIENTE] Costo IVATO: "+p.total_plus_recharge_iva+" &euro;";
										  
							if (settings.price_handling_calc_recharge)
								details += "<br />Categoria: "+(p.cat||'non disponibile')+"<br />Ricarico percentuale: "+(p.recharge)+"%<br /> \
										  Ricarico: "+(p.total_plus_recharge_iva-p.total_plus_iva).toFixed(2) + ' &euro;';

							if (settings.price_handling_calc_recharge) {
								if (settings.price_handling_calc_iva)
									cell.innerHTML = p.total_plus_recharge_iva + ' &euro;';
								else cell.innerHTML = p.total_plus_recharge + ' &euro; + IVA';
							} else {
								if (settings.price_handling_calc_iva)
									cell.innerHTML = p.total_plus_iva + ' &euro;'
								else cell.innerHTML = p.total + ' &euro; + IVA'
							}
							cell.innerHTML = cell.innerHTML + "<br /><img onclick='Tip(\""+details+"\");setTimeout(function(){UnTip()},"+tipTimeout+")' src='"+chrome.extension.getURL('/icons/point.png')+"' alt='*' />";
						});
						if ($$$('tr.grey th').length)
							$$$('tr.grey th')[3].style.width = '70px';
					}
					
					/***********************************************/
					/*** CALCULATE AND GIVE CART PRODUCTS TOTALS ***/
					/***********************************************/
					if (settings.price_handling) {
						cart_totals = getTotalsFromProducts(products);
						if ($$$('strong:contains("Totale")').length) {
							var cell = $$$('strong:contains("Totale")').parent().parent().children()[1];
							cell.colSpan = 3;
							cell.vAlign = 'top';
							$$$('strong:contains("Totale")').parent().parent().children()[0].vAlign = 'top';
							if (settings.price_handling_calc_recharge) {
								if (settings.price_handling_calc_iva) {
									$$$('strong:contains("Totale")')[0].innerHTML = "Totale Fattura:<br />Imponibile:<br />IVA:";
									cell.innerHTML = cart_totals[6].toFixed(2) + " &euro;<br />"+cart_totals[5].toFixed(2) + " &euro;<br />"+ cart_totals[4].toFixed(2) + " &euro;";
								} else { 
									$$$('strong:contains("Totale")')[0].innerHTML = "Imponibile:";
									cell.innerHTML = cart_totals[6].toFixed(2) + " &euro; + IVA";
								}
							} else {
								if (settings.price_handling_calc_iva) {
									$$$('strong:contains("Totale")')[0].innerHTML = "Totale Fattura:<br />Imponibile:<br />IVA:";
									cell.innerHTML = cart_totals[2].toFixed(2) + " &euro;<br />"+cart_totals[0].toFixed(2) + " &euro;<br />"+ cart_totals[1].toFixed(2) + " &euro;";
								} else { 
									$$$('strong:contains("Totale")')[0].innerHTML = "Imponibile:";
									cell.innerHTML = cart_totals[0].toFixed(2) + " &euro; + IVA";
								}
							}
							
							var details = "Costo: "+cart_totals[0]+" &euro;<br /> \
										  IVA: "+cart_totals[1]+" &euro;<br /> \
										  Costo IVATO: "+cart_totals[2]+" &euro;<br /> \
										  [CLIENTE] Costo: "+cart_totals[5]+" &euro;<br /> \
										  [CLIENTE] IVA: "+cart_totals[4]+" &euro;<br /> \
										  [CLIENTE] Costo IVATO: "+cart_totals[6]+" &euro;";
										  
							if (settings.price_handling_calc_recharge)
								details += "<br />Ricarico: "+(cart_totals[6]-cart_totals[2]).toFixed(2)+" &euro;";
							cell.innerHTML = cell.innerHTML + "<br /><img onclick='Tip(\""+details+"\");setTimeout(function(){UnTip()},"+tipTimeout+")' src='"+chrome.extension.getURL('/icons/point.png')+"' alt='*' />";
						}
					}
				}
			}
		}, 10);
		console.log("Brevizer cart ends.");
	}
});
if(chrome.runtime.lastError) { console.error(chrome.runtime.lastError); }