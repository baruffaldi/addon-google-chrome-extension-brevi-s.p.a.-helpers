this.manifest = {
    "name": "Brevizer",
    "icon": "icon.png",
    "settings": [
        {
            "tab": i18n.get("extension"),
            "group": i18n.get("status"),
            "name": "enabled",
            "type": "checkbox",
            "label": i18n.get("enabled")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("minimal"),
            "name": "ui_minimal",
            "type": "checkbox",
            "label": i18n.get("enable")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("minimal"),
            "name": "ui_minimal_description",
            "type": "description",
            "text": i18n.get("ui_minimal_description")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("advanced"),
            "name": "ui_wide",
            "type": "checkbox",
            "label": i18n.get("wide")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("advanced"),
            "name": "ui_enhanced_view",
            "type": "checkbox",
            "label": i18n.get("enhanced_view")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("advanced"),
            "name": "ui_listing_categories_alpha",
            "type": "checkbox",
            "label": i18n.get("ui_listing_categories_alpha")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("listing"),
            "name": "ui_listing_promo",
            "type": "radioButtons",
			"options": [
				{"text": i18n.get("enabled"), "value": "y"},
				{"text": i18n.get("disabled"), "value": ""}
			],
            "label": i18n.get("ui_listing_promo")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("listing"),
            "name": "ui_listing_light",
            "type": "radioButtons",
			"options": [
				{"text": i18n.get("enabled"), "value": "y"},
				{"text": i18n.get("disabled"), "value": "n"},
				{"text": i18n.get("notset"), "value": ""}
			],
            "label": i18n.get("ui_listing_light")
        },/*
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("listing"),
            "name": "ui_listing_availability",
            "type": "radioButtons",
			"options": [
				{"text": i18n.get("available"), "value": "y"},
				{"text": i18n.get("all"), "value": "n"},
				{"text": i18n.get("notset"), "value": ""}
			],
            "label": i18n.get("ui_listing_availability")
        },*/
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("listing"),
            "name": "ui_listing_autosort",
            "type": "radioButtons",
			"options": [
				{"text": i18n.get("ascending"), "value": "dn"},
				{"text": i18n.get("descending"), "value": "up"},
				{"text": i18n.get("notset"), "value": ""}
			],
            "label": i18n.get("ui_listing_autosort")
        },
        {
            "tab": i18n.get("ui"),
            "group": "Logo",
            "name": "ui_logo_url",
            "type": "text",
            "label": "URL Logo",
            "text": "http://example.com/images/logo.png"
        },
        {
            "tab": i18n.get("ui"),
            "group": "Logo",
            "name": "ui_logo_replace_description",
            "type": "description",
            "text": i18n.get("ui_logo_replace_description")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("hide"),
            "name": "ui_hide_ads",
            "type": "checkbox",
            "label": i18n.get("ads")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("hide"),
            "name": "ui_hide_news",
            "type": "checkbox",
            "label": i18n.get("news")
        },
        {
            "tab": i18n.get("ui"),
            "group": i18n.get("hide"),
            "name": "ui_hide_logo",
            "type": "checkbox",
            "label": "Logo"
        },
		
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("prices_auto"),
            "name": "price_handling",
            "type": "checkbox",
            "label": i18n.get("price_handling")
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("prices_auto"),
            "name": "price_handling_description",
            "type": "description",
            "text": i18n.get("price_handling_description")
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("general"),
            "name": "price_handling_calc_brevi_iva",
            "type": "checkbox",
            "label": i18n.get("price_handling_calc_brevi_iva")
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("general"),
            "name": "price_handling_brevi_iva",
            "type": "slider",
            "label": i18n.get("iva"),
            "max": 100,
            "min": 0,
            "step": 1,
            "display": true,
            "displayModifier": function (value) {
                return value + "%";
            }
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("general"),
            "name": "price_handling_calc_recharge",
            "type": "checkbox",
            "label": i18n.get("price_handling_calc_recharge")
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("general"),
            "name": "price_handling_recharge",
            "type": "slider",
            "label": i18n.get("recharge"),
            "max": 100,
            "min": 0,
            "step": 1,
            "display": true,
            "displayModifier": function (value) {
                return value + "%";
            }
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("general"),
            "name": "price_handling_calc_iva",
            "type": "checkbox",
            "label": i18n.get("price_handling_calc_iva")
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("general"),
            "name": "price_handling_iva",
            "type": "slider",
            "label": i18n.get("iva"),
            "max": 100,
            "min": 0,
            "step": 1,
            "display": true,
            "displayModifier": function (value) {
                return value + "%";
            }
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("general"),
            "name": "price_min_limit",
            "type": "text",
            "label": i18n.get("price_min_limit"),
            "text": "50"
        },
		{
			"tab": i18n.get("carts"),
			"type": "description",
            "group": i18n.get("cart"),
			"text": ""
		},
		{
			"tab": i18n.get("carts"),
			"type": "button",
			"id": "delete_cart",
			"text": i18n.get("delete") + ' ' + i18n.get("cart")
		},
		{
			"tab": i18n.get("carts"),
			"type": "description",
            "group": i18n.get("saved_carts"),
			"text": ""
		},
		{
			"tab": i18n.get("carts"),
			"type": "button",
            "group": i18n.get("operations"),
			"id": "delete_all",
			"text": i18n.get("delete") + ' ' + i18n.get("all")
		},
        /*
		{
			"tab": i18n.get("carts"),
			"type": "button",
            "group": i18n.get("operations"),
			"id": "delete",
			"text": i18n.get("delete") + ' ' + i18n.get("carts")
		},
		{
			"tab": i18n.get("carts"),
			"type": "button",
            "group": i18n.get("operations"),
			"id": "load",
			"text": i18n.get("load") + ' ' + i18n.get("cart")
		},
		{
            "tab": i18n.get("prices"),
            "group": i18n.get("percents"),
            "name": "price_handling_iva",
            "type": "text",
            "label": i18n.get("iva"),
            "text": "0 - 100%"
        },
        {
            "tab": i18n.get("prices"),
            "group": i18n.get("percents"),
            "name": "price_handling_recharge",
            "type": "text",
            "label": i18n.get("recharge"),
            "text": "0 - 100%"
        },*/
        {
            "tab": "Login",
            "group": i18n.get("persistent_login"),
            "name": "persistent_login",
            "type": "checkbox",
            "label": i18n.get("enable")
        },
        {
            "tab": "Login",
            "group": i18n.get("persistent_login"),
            "name": "login_username",
            "type": "text",
            "label": i18n.get("username"),
            "text": i18n.get("x-characters")
        },
        {
            "tab": "Login",
            "group": i18n.get("persistent_login"),
            "name": "login_password",
            "type": "text",
            "label": i18n.get("password"),
            "text": i18n.get("x-characters-pw"),
            "masked": true
        },
        {
            "tab": "Login",
            "group": i18n.get("persistent_login"),
            "name": "persistent_login_description",
            "type": "description",
            "text": i18n.get("persistent_login_description")
        },
        {
            "tab": i18n.get("credits"),
            "name": "credits_description",
            "type": "description",
            "text": i18n.get("credits_description")
        },
		{
            "tab": "hidden",
            "name": "price_handling_categories",
            "type": "text",
            "display": false
        },
		
		
		
        /*{
            "tab": "hidden",
            "name": "price_handling_categories",
            "type": "text",
            "display": false
        },
		{
            "tab": i18n.get("information"),
            "group": i18n.get("logout"),
            "name": "myCheckbox",
            "type": "checkbox",
            "label": i18n.get("enable")
        },
        {
            "tab": i18n.get("information"),
            "group": i18n.get("logout"),
            "name": "myButton",
            "type": "button",
            "label": i18n.get("disconnect"),
            "text": i18n.get("logout")
        },
        {
            "tab": i18n.get("details"),
            "group": "Sound",
            "name": "noti_volume",
            "type": "slider",
            "label": "Notification volume:",
            "max": 1,
            "min": 0,
            "step": 0.01,
            "display": true,
            "displayModifier": function (value) {
                return (value * 100).floor() + "%";
            }
        },
        {
            "tab": i18n.get("details"),
            "group": "Sound",
            "name": "sound_volume",
            "type": "slider",
            "label": "Sound volume:",
            "max": 100,
            "min": 0,
            "step": 1,
            "display": true,
            "displayModifier": function (value) {
                return value + "%";
            }
        },
        {
            "tab": i18n.get("details"),
            "group": "Food",
            "name": "myPopupButton",
            "type": "popupButton",
            "label": "Soup 1 should be:",
            "options": {
                "groups": [
                    "Hot", "Cold",
                ],
                "values": [
                    {
                        "value": "hot",
                        "text": "Very hot",
                        "group": "Hot",
                    },
                    {
                        "value": "Medium",
                        "group": 1,
                    },
                    {
                        "value": "Cold",
                        "group": 2,
                    },
                    ["Non-existing"]
                ],
            },
        },
        {
            "tab": i18n.get("details"),
            "group": "Food",
            "name": "myListBox",
            "type": "listBox",
            "label": "Soup 2 should be:",
            "options": [
                ["hot", "Hot and yummy"],
                ["cold"]
            ]
        },
        {
            "tab": i18n.get("details"),
            "group": "Food",
            "name": "myRadioButtons",
            "type": "radioButtons",
            "label": "Soup 3 should be:",
            "options": [
                ["hot", "Hot and yummy"],
                ["cold"]
            ]
        }*/
    ],
    "alignment": [
        [
            "login_username",
            "login_password"
        ],
        /*[
            "noti_volume",
            "sound_volume"
        ]*/
    ]
};
