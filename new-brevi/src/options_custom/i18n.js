// SAMPLE
this.i18n = {
    "settings": {
        "en": "Settings",
        "de": "Optionen",
		"it": "Opzioni"
    },
    "search": {
        "en": "Search",
        "de": "Suche",
		"it": "Cerca"
    },
    "nothing-found": {
        "en": "No matches were found.",
        "de": "Keine Übereinstimmungen gefunden.",
		"it": "Nessun risultato trovato."
    },
    
    
    "extension": {
        "en": "Extension",
		"it": "Estensione"
    },
    "status": {
        "en": "Status",
		"it": "Stato"
    },
    "ui": {
        "en": "Interface",
		"it": "Interfaccia"
    },
    "wide": {
        "en": "Wide screen",
		"it": "Schermo largo"
    },
    "shop": {
        "en": "Shop",
		"it": "Negozio"
    },
    "minimal": {
        "en": "Minimal",
		"it": "Minimalistica"
    },
    "ui_minimal_description": {
        "en": "The minimal interface literally give you a very light\
		user interface by removing all non-necessary stuff from the pages.",
		"it": "L'interfaccia minimale ti da la possibilita' di navigare il \
		negozio con un interfaccia leggerissima.<br />Questo é permesso grazie \
		alla preventiva rimozione di tutti gli elementi non utili all'acquisto."
    },
    "ui_logo_replace_description": {
        "en": "Insert here the URL of your company's logo. This will be replaced \
		in place of original logo.",
		"it": "Inserisci quì l'indirizzo del logo della tua attività. Questo verrà\
		inserito al posto di quello originale."
    },
    "hide": {
        "en": "Hide",
		"it": "Nascondi"
    },
    "news": {
        "en": "Latest news",
		"it": "Ultime notizie"
    },
    "ads": {
        "en": "Advertisement",
		"it": "Pubblicità"
    },
    "advanced": {
        "en": "Advanced options",
		"it": "Opzioni avanzate"
    },
    "listing": {
        "en": "Products listing",
		"it": "Liste prodotti"
    },
    "ui_listing_promo": {
        "en": "Show only products which makes you save money",
		"it": "Visualizza soltanto i prodotti in promozione"
    },
    "ui_listing_autosort": {
        "en": "Auto sort listing results",
		"it": "Ordinamento automatico"
    },
    "ui_listing_light": {
        "en": "Minimal results interface",
		"it": "Interfaccia minimale dei risultati"
    },
    "ui_listing_availability": {
        "en": "Products availability",
		"it": "Disponibilità prodotti"
    },
    "enhanced_view": {
        "en": "Optimized interface",
		"it": "Interfaccia ottimizzata"
    },
    "price_handling": {
        "en": "Automatic calculate prices",
		"it": "Calcola prezzi in automatico"
    },
    "notset": {
        "en": "Not set",
		"it": "Indifferente"
    },
    "prices_auto": {
        "en": "Automatic prices",
		"it": "Prezzi automatici"
    },
    "price_min_limit": {
        "en": "Minimal order cash amount",
		"it": "Importo minimo per ordine"
    },
    "price_handling_description": {
        "en": "This will add vat value, total price and total price \
		plus your recharge on products beside its original prices.",
		"it": "Abilitando questa opzione verranno visualizzati a fianco \
		al prezzo originale: IVA, totale e totale piu' il tuo ricarico \
		sul prodotto."
    },
    "price_handling_calc_recharge": {
        "en": "Automatic calculate recharge",
		"it": "Calcola automaticamente il ricarico"
    },
    "price_handling_calc_iva": {
        "en": "Automatic calculate VAT",
		"it": "Calcola automaticamente l'IVA"
    },
    "price_handling_calc_brevi_iva": {
        "en": "Automatic calculate Brevi's VAT",
		"it": "Calcola automaticamente l'IVA di Brevi"
    },
    "date": {
        "en": "Date",
		"it": "Data"
    },
    "items": {
        "en": "Items",
		"it": "Unità"
    },
    "ui_listing_categories_alpha": {
        "en": "Sort categories menu",
		"it": "Menu' categorie ordinato"
    },
    "total": {
        "en": "Total",
		"it": "Totale"
    },
    "products": {
        "en": "Products",
		"it": "Prodotti"
    },
    "quantity": {
        "en": "Q.ty",
		"it": "Quantità"
    },
    "products_quantity": {
        "en": "Products Q.ty",
		"it": "Quantità Prodotti"
    },
    "price": {
        "en": "Price",
		"it": "Costo"
    },
    "total_price": {
        "en": "Customer Price",
		"it": "Costo Cliente"
    },
    "cart": {
        "en": "Cart",
		"it": "Carrello"
    },
    "carts": {
        "en": "Carts",
		"it": "Carrelli"
    },
    "name": {
        "en": "Name",
		"it": "Nome"
    },
    "article": {
        "en": "Article",
		"it": "Articolo"
    },
    "names": {
        "en": "Names",
		"it": "Nomi"
    },
    "articles": {
        "en": "Articles",
		"it": "Articoli"
    },
    "saved_carts": {
        "en": "Saved carts",
		"it": "Carrelli salvati"
    },
    "load": {
        "en": "Load",
		"it": "Carica"
    },
    "delete": {
        "en": "Delete",
		"it": "Cancella"
    },
    "operations": {
        "en": "Operations",
		"it": "Operazioni"
    },
    "iva": {
        "en": "VAT",
		"it": "IVA"
    },
    "general": {
        "en": "General",
		"it": "Generale"
    },
    "prices": {
        "en": "Prices",
		"it": "Prezzi"
    },
    "percent": {
        "en": "Percent",
		"it": "Percentuale"
    },
    "percents": {
        "en": "Percents",
		"it": "Percentuali"
    },
    "default_percents": {
        "en": "Default percents",
		"it": "Percentuali"
    },
    "categories": {
        "en": "Categories",
		"it": "Categorie"
    },
    "recharge": {
        "en": "Recharge",
		"it": "Ricarico"
    },
    "persistent_login": {
        "en": "Persistent login",
		"it": "Login persistente"
    },
    "persistent_login_description": {
        "en": "This option allows you to automatically login when the site disconnect \
		you because idling/timeout.",
		"it": "Questa opzione ti permette di effettuare automaticamente il login \
		qualora venissi scollegato per idle/timeout."
    },
    "login": {
        "en": "Login",
        "de": "Anmeldung",
		"it": "Entra"
    },
    "username": {
        "en": "Username:",
        "de": "Benutzername:",
		"it": "Utente:"
    },
    "password": {
        "en": "Password:",
        "de": "Passwort:",
		"it": "Password:"
    },
    "x-characters": {
        "en": "6 - 12 characters",
        "de": "6 - 12 zeichen",
		"it": "6 - 12 caratteri"
    },
    "x-characters-pw": {
        "en": "10 - 18 characters",
        "de": "10 - 18 zeichen",
		"it": "10 - 18 caratteri"
    },
    "credits": {
        "en": "Credits",
		"it": "Crediti"
    },
    "credits_description": {
        "en": "Information",
		"it": "Questa estensione é stata sviluppata seguendo la filosofia opensource da \
		<a href='http://www.baruffaldi.info' target='_blank'>Filippo Baruffaldi</a> \
		della <a href='http://www.gbconsulting.it' target='_blank'>GB Consulting Italia</a> \
		per semplificare la vita ai clienti di <a href='http://www.brevi.it' target='_blank'>Brevi S.p.A.</a>.\
		<br /><br />I sorgenti sono reperibili nel <a href='http://repository.gbconsulting.it/addons-chrome-extension-brevi' target='_blank'>repository</a> aziendale.<br /><br /> \
		Tutti i nomi, loghi e marchi visualizzati su questa estensione, sono dei rispettivi proprietari. L'unico brand di cui si dichiarano i copyrights è quello della GB Consulting."
    },
	
	
    "information": {
        "en": "Information",
        "de": "Information",
		"it": "Informazioni"
    },
    "description": {
        "en": "This is a description. You can write any text inside of this.<br>\
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut\
        labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores\
        et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem\
        ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et\
        dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.\
        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        
        "de": "Das ist eine Beschreibung. Du kannst hier beliebigen Text einfügen.<br>\
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut\
        labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores\
        et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem\
        ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et\
        dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.\
        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
		
		"it": "Blah Blah"
    },
    "logout": {
        "en": "Logout",
        "de": "Abmeldung",
		"it": "Esci (logout)"
    },
    "ascending": {
        "en": "Ascending",
		"it": "Crescente"
    },
    "descending": {
        "en": "Descending",
		"it": "Decrescente"
    },
    "all": {
        "en": "All",
		"it": "Tutti"
    },
    "available": {
        "en": "Available",
		"it": "Disponibile"
    },
    "enable": {
        "en": "Enable",
        "de": "Aktivieren",
		"it": "Abilita"
    },
    "disable": {
        "en": "Disable",
		"it": "Disabilita"
    },
    "enabled": {
        "en": "Enabled",
		"it": "Abilitato/a"
    },
    "disabled": {
        "en": "Disabled",
		"it": "Disabilitato/a"
    },
    "disconnect": {
        "en": "Disconnect:",
        "de": "Trennen:",
		"it": "Disconnetti"
    },
    "details": {
        "en": "Details:",
		"it": "Dettagli"
    }
};
