var tipTimeout = 5000,
	b_settings = new Store("settings", {}).toObject(),
	b_carts = new Store("carts", {}),
    b_carts_details = new Store("carts_details", {}),
	carts = b_carts.toObject(),
	carts_details = b_carts_details.toObject(),
	parseProducts = function(products){
		if (products.length)
			products.each(function(p,a){
				if (!b_settings["price_handling_category_"+products[a].cat])
					products[a].recharge = b_settings.price_handling_recharge;
				else products[a].recharge = b_settings["price_handling_category_"+products[a].cat];
				products[a].price_iva = parseFloat((p.price / 100 * parseInt(b_settings.price_handling_iva)).toFixed(2));
				products[a].price_plus_iva = parseFloat((products[a].price + products[a].price_iva).toFixed(2));
				products[a].price_recharge = parseFloat((p.price / 100 * parseInt(products[a].recharge)).toFixed(2));
				products[a].price_recharge_iva = parseFloat(((products[a].price + products[a].price_recharge) / 100 * parseInt(b_settings.price_handling_iva)).toFixed(2));
				products[a].price_plus_recharge = parseFloat((products[a].price + products[a].price_recharge).toFixed(2));
				products[a].price_plus_recharge_iva = parseFloat((products[a].price + products[a].price_recharge + products[a].price_recharge_iva).toFixed(2));
				
				products[a].total_recharge = parseFloat((products[a].price_recharge * parseInt(p.quantity)).toFixed(2)) || products[a].price_recharge;
				products[a].total_iva = parseFloat((products[a].price_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_iva;
				products[a].total_recharge_iva = parseFloat((products[a].price_recharge_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_recharge_iva;
				products[a].total_plus_recharge = parseFloat((products[a].price_plus_recharge * parseInt(p.quantity)).toFixed(2)) || products[a].price_plus_recharge;
				products[a].total_plus_iva = parseFloat((products[a].price_plus_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_plus_iva;
				products[a].total_plus_recharge_iva = parseFloat((products[a].price_plus_recharge_iva * parseInt(p.quantity)).toFixed(2)) || products[a].price_plus_recharge_iva;
				products[a].total = parseFloat((p.price * p.quantity).toFixed(2)) || p.price;
			});
		return products;
	},
	cleanCart = function(){
		var xhr = new XMLHttpRequest(),
			params = "svuota.x=96&svuota.y=26&svuota=Svuota%20tutto%20il%20carrello";
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4)
			{
				alert('carrello svuotato');
			}
		}
		xhr.open('POST', 'http://www.brevi.it/carrello', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
		xhr.send(params);
	},
	addToCart = function(id, quantity){
		var xhr = new XMLHttpRequest(),
			params = "car%5B"+id+"%5D="+quantity+"&compr%5B"+id+"%5E1%5D.x=18&compr%5B"+id+"%5E1%5D.y=7&compr%5B"+id+"%5E1%5D=Compra";
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4)
			{
			}
		}
		xhr.open('POST', 'http://www.brevi.it/commerce.htm?ref=logo', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
		xhr.send(params);
	};

window.addEvent("domready", function () {
    new FancySettings.initWithManifest(function (settings) {
		$$('#tab-container div.tab').each(function(b,a){
			if (b.innerText == 'hidden')
			  b.style.display = 'none';
		});
		
        if (typeof settings.manifest.price_handling_categories.element.value != 'undefined') {
			var categories = JSON.parse(settings.manifest.price_handling_categories.element.value);
			for (var x=0;x<=categories.length-1;x++) {
				var category = categories[x];
				settings.create({
					"tab": i18n.get("prices"),
					"group": i18n.get("categories"),
					"name": "price_handling_category_"+category.id,
					"type": "slider",
					"label": category.name,
					"max": 1000,
					"id": "price_handling_category_"+category.id,
					"min": -1,
					"value": -1,
					"step": 1,
					"display": true,
					"displayModifier": function (value) {
						if (value == 0 && typeof (new Store("settings", {}).toObject()["price_handling_category_"+category.id]) == 'undefined')
							return 'Disabilitato';
						if (value >= 0)
							return value + "%";
						return 'Disabilitato';
					}
				});
			}
		}
		
		var cart = b_carts.get('cart');

		if (typeof cart != 'undefined' && cart != "[]") {
			var cart_table_container = $$('.tab-content:contains("Carrelli") > .group > tr > .group-content')[0];
			cart_table_container.innerHTML += "<table id='cart'><tr><th>"+i18n.get("ID")+"</th><th>"+i18n.get("articles")+"</th><th style='width:50px'>"+i18n.get("quantity")+"</th><th>"+i18n.get("price")+"</th><th>"+i18n.get("total_price")+"</th></tr></table>";
			var ct = parseProducts(JSON.parse(cart)),
				details = "",
				timestamp = 0,
				date = (new Date(timestamp)).toLocaleDateString() + ' ' + (new Date(timestamp)).toLocaleTimeString(),
				products = ct.length,
				products_quantity = 0,
				price = 0,
				total_price = 0;
				
			for (var x=0;x<=ct.length-1;x++) {
				var product = ct[x];
				products_quantity += product.quantity;
				price += product.total;
				total_price += product.total_plus_recharge_iva;
				info = "<input type='image' style='display:none;' name='0' value=\""+product.desc+"\" tip='false' id='hoverinfo_"+product.id+"' src='/icons/point.png' alt='*' />";
				details += '<tr><td>' + info + product.id + '</td><td>' + product.title + '</td><td>'+product.quantity+'</td><td>' + product.total.toFixed(2) + '&euro;</td><td>' + product.total_plus_recharge_iva.toFixed(2) + '&euro;</td></tr>';
			}
			
			
			//info = "<input type='image' name='0' value=\"Tip($('details_"+timestamp+"').innerHTML)\" tip='false' id='info_"+timestamp+"' src='/icons/point.png' alt='*' />"
			details += "<tr id='cart_totals'><td><b>Prodotti</b><br />"+products+"</td><td colspan='2' style='text-align:right'><b>"+i18n.get("total")+" "+i18n.get("items")+"</b><br />"+products_quantity+"</td><td><b>"+i18n.get("total")+"</b><br />"+price.toFixed(2)+"&euro;</td><td><b>"+i18n.get("total")+"</b><br />"+total_price.toFixed(2)+"&euro;</td></td></tr>";
			$('cart').innerHTML += details;
		}
		
		var gottaGo = false;
		for (var i in carts)
			if (i != 'cart')
				gottaGo = true;
				
		if (gottaGo) {
			var carts_table_container = $$('.tab-content:contains("Carrelli") > .group > tr > .group-content')[2];
			carts_table_container.innerHTML += "<table id='carts'><tr><th>"+i18n.get("name")+"</th><th>"+i18n.get("date")+"</th><th>"+i18n.get("details")+"</th><th style='width:50px'>"+i18n.get("products")+"</th><th style='width:50px'>"+i18n.get("products_quantity")+"</th><th>"+i18n.get("price")+"</th><th>"+i18n.get("total_price")+"</th><th></th><th></th></tr></table>";
			for (var cart_id in carts) {
				if (cart_id != 'cart') {
					var crt = parseProducts(JSON.parse(carts[cart_id])),
						details = "<table>",
						timestamp = parseInt(cart_id.substr(4)),
						date = (new Date(timestamp)).toLocaleDateString() + ' ' + (new Date(timestamp)).toLocaleTimeString(),
						products = crt.length,
						products_quantity = 0,
						price = 0,
						total_price = 0;

					for (var x=0;x<=crt.length-1;x++) {
						var product = crt[x];
						products_quantity += product.quantity;
						price += product.total;
						total_price += product.total_plus_recharge_iva;
						details += "<tr><td>" + product.total.toFixed(2) + '&euro;</td><td>' + product.total_plus_recharge_iva.toFixed(2) + '&euro;</td><td>' + product.id + '</td><td>' + product.title + ' ' + product.desc + '</td></tr>';
					}
					
					details += "</table>";
					
					//info = "<input type='image' name='0' value=\"Tip($('details_"+timestamp+"').innerHTML)\" tip='false' id='info_"+timestamp+"' src='/icons/point.png' alt='*' />"
					info = "<input type='image' name='0' value=\"Tip($('details_"+timestamp+"').innerHTML)\" tip='false' id='info_"+timestamp+"' src='/icons/point.png' alt='*' />"
					load_button = "<input type='button' value='"+i18n.get("load")+"' id='load_"+timestamp+"' name='load_"+timestamp+"' />";
					delete_button = "<input type='button' value='"+i18n.get("delete")+"' id='delete_"+timestamp+"' name='delete_"+timestamp+"' />";
					$('carts').innerHTML += "<tr id='row_"+timestamp+"'><td><input name='name_"+timestamp+"' id='name_"+timestamp+"' value='' /></td><td>"+date+"</td><td>"+info+"<div style='display:none;' id='details_"+timestamp+"'>"+details+"</div></td><td>"+products+"</td><td>"+products_quantity+"</td><td>"+price.toFixed(2)+"&euro;</td><td>"+total_price.toFixed(2)+"&euro;</td><td>"+load_button+"</td><td>"+delete_button+"</td></tr>";
				}
			}
		}
		
		$$('[id^="hoverinfo"]').each(function(el,a){
			var eli = el.parentElement.parentElement;
			eli.addEvent('mouseover', function(){
				Tip(el.value);
			});
			eli.addEvent('mouseout', function(){
				UnTip();
			});
		});
		$$('[id^="info"]').each(function(el,a){
			var tmp = el.value;
			el.addEvent('click', function(){
				if (el.name == '0') {
					el.name = '1';
					eval(tmp);
				} else {
					el.name = '0';
					UnTip();
				}
			});
		});
		$$('[id*="name"]').each(function(el,a){
			var id = el.id.substr(5);
			el.value = b_carts_details.get(id)||'';
			el.addEvent('keyup', function(a){
				b_carts_details.set(id, a.target.value);
			});
		});
		$$('[id*="load"]').each(function(el,a){
			var id = el.id.substr(5);
			
			el.addEvent('click', function(){
				cleanCart();
				JSON.parse(carts['cart'+id]).each(function(product,b){
					addToCart(product.id, product.quantity);
				});
				alert('Carrello importato');
			});
		});
		$$('[id*="delete_"]').each(function(el,a){
			var id = el.id.substr(7);
			if (el.id != 'delete_all' && el.id != 'delete_cart')
				el.addEvent('click', function(){
					b_carts.remove('cart'+id);
					$('row_'+id).destroy();
				});
		});
		
		$('delete_cart').addEvent('click', function(){
			cleanCart();
		});
		
		$$('#carts tr th').setStyle('font-size', '14px');
		$$('#carts tr td').setStyle('text-align', 'center');
		$$('#carts tr td:first-child').setStyle('text-align', 'left');
    });
});
