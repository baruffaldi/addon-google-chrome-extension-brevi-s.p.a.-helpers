/* TODO
AREA LISTING
- Calcolo SIAE, sconti, promozioni e contributi vari
- Aggiunta dell'IVA da conservare e il ricarico netto (+lordo) (22% del ricarico)
- Aggiunta del calcolo dell'IVA da rivendere
- Valore IVA Brevi e IVA Ditta
- Informazioni Societ� - Manodopera - Etc
- Redirigere automaticamente il preventivo (da pagina carrello o settings o popup)
- Se cancelli tutti o singoli ordini, cancellare relativi nomi carrelli

AREA POPUP
- indicatori configurazione
- Riportare tabelle opzioni su popup

Copyrights
Migliori descrizioni settings
Inserimento Loghi e rispettivi proprietari
Rifare la grafica


HOTFIX DEL SITO E FUNZIONI FUTURE
- Inserimento calcolo distinto IVA brevi (come su settaggi)
- Scelta del campo da visualizzare
- Gestione del calcolo sulle varie promo etc
- Controllo del tasto carrello che diventi verde solo al raggiungimento dei 50 euro minimi (senza ricarichi iva etc)
- Aggiungere tasto Checkout a fianco al carrello con popup (conferma) (solo se � verde)
- Abilitabile, se modifichi il campo per il quantity, viene modificato il campo prezzo aggiungendo il totale
- Modalit� SELF-SHOP(serie di opzioni auto-selezionate)
- Analisi di tutti gli href
-- Salvataggio destinazione (in caso di 403, login e poi redirect automatico )
-- Controllo variabili per il listing
- Gestione del filtro "non disponibili/disponibili-in-sede/disponibili"
- Interfaccia migliorata? (tramite css embeddabile)
- redirect handler
- Migliore gestione dei filtri sul listing (modifica dei href?)
- Carica singoli prodotti
*/

/*
* Estensione verificata da Google ed open-source, i sorgenti infatti sono disponibili all'indirizzo http://repository.gbconsulting.it/chrome-extension-brevi
*
* Dopo il settaggio delle opzioni nella pagina Opzioni dell'estensione, sar� possibile usare l'estensione sul sito.
*
* L'estensione far� tutto in automatico e sar� gestibile sia dalla pagina Opzioni che dalla finestra che compare al click sull'icona dell'estensione Brevizer in alto a destra nel browser.
*
* Sulla finestra che compare, sar� possibile visualizzare gli indicatori di stato della configurazione scelta, il carrello attuale e tutti i carrelli salvati.
*
* Disponibile in due lingue (Italiano e Inglese). Se sei madrelingua e vuoi la traduzione nella tua lingua, contattami e inseriremo insieme il nuovo idioma.
*
* ELENCO FUNZIONI
*
* Gestione interfaccia
* - Interfaccia minimale
* - Interfaccia widescreen
* - Interfaccia ottimizzata
* - - Colorazione tasto carrello a seconda dell'ammontare del carrello, se supera il minimo consentito, lo colora di verde
* - - Immagini di anteprima ingrandite
* - Cambio logo
* - Possibilit� di nascondere news, pubblicit�, logo
* - Si possono configurare staticamente i parametri per
* - - Ordinamento lista prodotti
* - - Lista prodotti leggera
* - - Visualizzazione prodotti non disponibili
* - Tasto "promozioni" aggiunto al menu principale
*
* Gestione Prezzi
* - Impostazione IVA e Ricarico in percentuale
* - Possibilit� di inserire percentuale di ricarico specifico su specifiche categorie
* - Funzione(disabilitabile) di calcolo automatico dell'IVA e del ricarico, visibile in tendina nascosta a fianco al prezzo totale visibile
* - Funzione(disabilitabile) di calcolo automatico del totale, comprensivo del ricarico (visibile solo passandoci sopra col mouse e disabilitabile in modalit� "self-shop")
*
* Gestione Carrelli
* - Gestione dei carrelli per salvataggio, visualizzazione, eliminazione, caricamento.
* - Al salvataggio del carrello, viene richiesto se svuotarlo per preparare un altro carrello da salvare
* - Salvataggio automatico del carrello in tempo reale sullo storage locale (piu' sicuro dei cookies e backupabile)
* - Possibilit� di assegnare nomi ai carrelli salvati
*
* Gestione Utenza
* - Possibilit� di configurare utente e password per far si che in caso di logout automatico del sito, l'estensione fa' il login per te.
*
*/

var b_settings = new Store("settings", {
	"enabled": true,
    "brevizer": new Date()+'',
	"price_handling_iva": 22,
	"price_handling_recharge": 30,
	"price_min_limit": 50
});
var b_carts = new Store("carts", {});

chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
  	//chrome.pageAction.show(sender.tab.id);
	if (typeof request.getSettings != 'undefined')
		if (request.getSettings)
			try { sendResponse({settings:b_settings.toObject()}); }
			catch(e){sendResponse(false);return;}
		
	if (typeof request.setOption != 'undefined')
		if (request.setOption) {
			try { b_settings.set(request.setOption['name'], request.setOption['value']); }
			catch(e){sendResponse(false);return;}
			sendResponse(true);
		}
		
	if (typeof request.getCarts != 'undefined')
		if (request.getCarts) {
			try { sendResponse(b_carts.toObject()); }
			catch(e){sendResponse(false);return;}
			sendResponse(true);
		}
	if (typeof request.getCart != 'undefined')
		if (request.getCart) {
			try { sendResponse(b_carts.get(request.getCart)); }
			catch(e){sendResponse(false);return;}
			sendResponse(true);
		}
	if (typeof request.addCart != 'undefined')
		if (request.addCart) {
			try { b_carts.set(request.addCart[0], request.addCart[1]); }
			catch(e){sendResponse(false);return;}
			sendResponse(true);
		}
	if (typeof request.modifyCart != 'undefined')
		if (request.modifyCart) {
			try { b_carts.set(request.modifyCart[0], request.modifyCart[1]); }
			catch(e){sendResponse(false);return;}
			sendResponse(true);
		}
	if (typeof request.delCarts != 'undefined')
		if (request.delCarts) {
			try { b_carts.removeAll(); }
			catch(e){sendResponse(false);return;}
			sendResponse(true);
			sendResponse(true);
		}
	if (typeof request.delCart != 'undefined')
		if (request.delCart) {
			try { b_carts.remove(request.delCart); }
			catch(e){sendResponse(false);return;}
			sendResponse(true);
			sendResponse(true);
		}
  });